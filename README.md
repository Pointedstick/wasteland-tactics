# The game
Wasteland Tactics is a tabletop wargame simulating the fast-paced randomness and tactical decision-making of a real battlefield.

The rules are fundamentally simple to encourage fast play, yet combine together in deep and interesting ways. The game can be played at any scale: a skirmish between ragtag bands of desperadoes will take under an hour to play, while a company-scale battle with hundreds of soldiers and a dozen monsters and war machines is feasible in under four hours.

Features include:
- Compact set of basic rules that interact in complex ways
- Engaging and technically interesting player choices rather than endless sterile die rolling
- Interesting missions with complex, real-world style objectives
- Sophisticated leadership, morale, and unit suppression mechanics mimicking real battlefield psychology
- Alternating unit activation, reactions, single roll to hit/wound, and single save roll


# Who is it for?
Wasteland Tactics was written for the wargamer who wants to experience modern and sci-fi battles with tactical depth but without overcomplicated rules, who does not want to get locked into specific commercial games with their fast-paced and expensive upgrade cycles, and who has a tape measure. If you already have models and wargaming terrain, even better!


# How to get it

### Download the latest released version
[Click here](https://gitlab.com/Pointedstick/wasteland-tactics/-/jobs/5248497540/artifacts/file/rules.pdf) to download a PDF of the latest release of the rules.

### Download the latest unstable version
[Click here](https://gitlab.com/Pointedstick/wasteland-tactics/-/jobs/artifacts/master/file/rules.pdf?job=compile_pdf) to download a PDF of the latest unreleased, in-progress version of the rules. Note that the unreleased version may be buggy and is subject to change at any time!

### Build from source
The Wasteland Tactics rules are written using LaTeX. To compile them locally into PDF documents, you will need a LaTeX app or compiler. I use [KDE's Kile](https://apps.kde.org/en/kile) app.


# Policies, philosophy, and licensing
The goal of Wasteland Tactics is to make available a high quality 100% free open source wargaming ruleset. Accordingly, the rules will never use a freemium or DLC model with the "core" rules provided for free but extra "advanced rules" or "bonus content" costing money. The source code for 100% of the rules will always be free and open source under the GPLv3 license.

When the rules change, the goal is to improve the game by making it more fun, faster to play, and to reduce imbalances--never to temporarily give one faction an edge to encourage the sale of models, books, or anything else.

All rules changes will come in the form of discrete releases with a full set of release notes detailing what has changed, for your convenience.


# Commit message policy
Any commit that changes the rules to alter the gameplay in any way must have a commit message which begins with the text "`[rules]`". This does not apply to typo fixes or simple re-wordings; only to changes which alter the gameplay. This policy is intended to make it easy to generate a changelog of rules alterations for each release.
