
% Inherit standard global formatting
\input{globals/globals_book.tex}




\begin{document}

\title{Wasteland Tactics}

\begin{titlepage}
\maketitle
\end{titlepage}

\begin{multicols}{2}




\backmatter

\chapter*{Introduction}

% Remove page numbering on this page
\thispagestyle{empty}
% FIXME: hack to get page numbering to start with 1 on the next page
\pagenumbering{roman}

Wasteland Tactics is a tabletop wargame simulating the fast-paced randomness and tactical decision-making of a real battlefield--historical, modern, or in the far future. The game is played between 2 or more players using cardboard tokens, 10-sided dice, a tape measure, and a table-sized flat surface. Painted miniatures and 3D modeled terrain pieces can also be used.

The rules of Wasteland Tactics are simple to encourage fast play, but they combine together in deep and interesting ways. The game can be played at any scale: a skirmish between ragtag bands of desperadoes takes under an hour to play, while a company-scale battle with hundreds of soldiers and a dozen monsters and war machines is feasible in under four hours.

These game rules are free and open-source, licensed under the GNU GPLv3 license. This means that you are allowed to copy, modify, and redistribute them, as long as you include a copy of the source code used to generate the final product. Ongoing development takes place at \url{https://gitlab.com/Pointedstick/wasteland-tactics} and you are welcome to participate! Feel free to open \href{https://gitlab.com/Pointedstick/wasteland-tactics/-/issues}{Issues} for problems you encounter or suggestions for improvement, or submit \href{https://gitlab.com/Pointedstick/wasteland-tactics/-/merge_requests}{Merge Requests} with proposed changes.


\vfill\null
\columnbreak

\section*{Table of contents}

\toccontents




\chapter{Sequence of Play}

% FIXME: hack to get page numbering to start with 1 on this page
\pagenumbering{arabic}

\section*{1: Choose mission and forces}

The game is played between two sides, each represented by the Battle Group of a single player, or the Battle Groups of multiple players. Each side gets a total of 500 points shared among all its players to build Battle Groups consisting of 1-2 Command units, 2-6 Troops units, 0-2 Support units, 0-2 Spearhead units, 0-2 Fast units, 0-2 Heavy units, and 0-1 Transport unit for every non-\textit{Vehicle/Monster} unit taken.

Finally, each side determines their mission objectives (see the ``Missions'' section later).




\section*{2: Set up the battlefield}

The game may be played on any surface of at least 4 feet by 4 feet; 6' x 4' is ideal. Players take turns placing terrain pieces until a satisfactory battlefield has been assembled that feels thematically appropriate for the players' objectives, and each 2' x 2' area includes at least one large visibility-blocking piece of terrain. Players should mutually agree on the terrain's classification (see the ``Terrain and Cover'' section later).



\section*{3: Deploy forces}

There are two deployment zones: each a 10''deep strip emanating from one of the longer battlefield edges.

Players then determine initiative (see the next section) and go in reverse initiative order to deploy their battle groups within their deployment zones. Units cannot be deployed within 30'' of enemy units.

\textit{Leadership} units now receive their initial Order tokens.




\section*{4: Start the game}

\subsection*{Phase 1: Determine initiative}
Each player sums the numbers of the \textit{Leadership} rules for their deployed units. The player with the highest number has the initiative for this round.

\subsection*{Phase 2: Activate units}
The player with the initiative selects a unit to activate. If it has one Suppression token on it, it may perform a single Action (see the ``Actions and Reactions'' section later); if it has two or more, it performs no Actions and instead immediately deactivates. Otherwise it performs up to two Actions. The unit's second Action may be chosen after seeing the results of the first one. Then the unit deactivates.

Units with any Suppression tokens always benefit from the Defend action's bonuses, no matter what else they do.

After a unit deactivates, the player with the next-highest initiative value activates one of their units in the same manner. Once all players have activated one unit, the player with the initiative then activates a second unit, and so on. When a player runs out of units to activate, other players continue to activate units until they run out as well.

\subsection*{Phase 3: Morale and cleanup}
Once all units on the battlefield have been activated, each unit with any Suppression tokens makes a Will roll and discards one if it passes. After this, any unit that still has five or more Suppression tokens is immediately destroyed, its morale shattered!

After that, discard all Defend tokens and flip over all spent Order tokens. Play then proceeds to the next round.



\section*{Achieving victory}

The game ends after one side's forces are completely destroyed, or after 8 rounds. In this case, the side with the most victory points is the winner!




\chapter{Game Mechanics}

\section*{The golden rules}

More specific rules override more general ones.

If a dispute or a tie score arises during the course of play, go with whichever option would be the coolest. If this cannot be immediately agreed upon, or if both options are lame, settle the matter quickly with a game of rock-paper-scissors.



\section*{Units}

A unit can be a team of soldiers, a hero, a monster or vehicle, or any combination of them. All are represented by a rectangular base of any size between 1 and 1.5 square inches per starting HP. Individual painted models or even a small diorama can optionally be modeled on top. Because each unit's abilities are slightly abstracted, any models used to depict it need not achieve complete visual accuracy; being in the ballpark is fine, as long as it's clear to your opponents what the unit consists of!

Each unit's abilities are described by the following characteristics:
\begin{itemize}
    \item \textbf{Move:} Speed across the battlefield in inches.
    \item \textbf{Shoot:} Die roll needed to inflict damage with long-ranged shooting.
    \item \textbf{Assault:} Die roll needed to inflict damage in close-ranged firefights and melee combat.
    \item \textbf{Will:} Die roll needed to perform in stressful situations and cast or resist arcane powers.
    \item \textbf{Life:} Die roll needed to withstand being hit by enemy weapons.
    \item \textbf{HP:} Number of failed Life rolls required to destroy the unit.
    \item \textbf{Weapons:} The unit's weapons.
    \item \textbf{Special Rules:} Special rules permanently affecting the unit.
    \item \textbf{Type:} The unit's battlefield role: Command, Support, Spearhead, Troops, Fast, or Heavy.
\end{itemize}



\section*{Rolling dice}

During the game, units will ``make a Shoot roll,'' ``make a Life roll,'' and so on. To do this, roll a ten-sided die (known as a D10) and try to roll a number equal to or less than the characteristic in question.

For example, a unit with Life 6 passes Life rolls on a roll of 6 or less. If your dice show the number 0 instead of 10, treat 0 as 10.

Modifiers such as ``-2 Life'' or ``-1 Shoot'' modify the target number. For example if that model with 6 Life was struck with a Strength 2 weapon, the model would need to roll a 4 or less to survive. Modifiers always stack.

If a die roll's target number is 10 or more, it's an automatic success. Likewise, if the target number is 0 or less, it's an automatic failure.

If asked to roll a ``D5,'' roll a die and divide the number by 2, rounding up. Add modifiers after dividing. For example if rolling ``D5+2'' and the die shows a 9, the D5 roll is a 4, and adding 2 produces a final result of 6.



\section*{Measuring and visibility}

Distances are measured in inches. When measuring between two units, ``within X inches'' means that you measure the distance between the two closest visible points on their bases.

To assess visibility, choose a corner on the source unit's base and draw an imaginary line between that point and all four corners of the target unit's base. The lines are blocked by visibility-blocking terrain and other units at the same or higher level of elevation compared to either unit.

\begin{itemize}
    \item 0-1 lines blocked: target is \textbf{fully visible}
    \item 2-3 lines blocked: target is \textbf{partially visible}
    \item All 4 lines blocked: target is \textbf{not visible}
\end{itemize}



\section*{Engagement}

Two units are ``engaged'' when any parts of their bases are physically touching. If this is impossible because either unit is on an elevated piece of terrain, they count as being engaged when their bases are within 1'' and as close as possible.




\chapter{Missions}

Each side chooses one of the following four broad objectives. The side whose forces cost the fewest points gets to choose which mission they want; everyone else rolls a die to see which one they get:



\section*{Aggression}


\subsubsection*{1-2 Clear the Area:} Gain 6 victory points if there are no enemy Troops units in the area outside of either side's deployment zone.

\subsubsection*{3-4 Fire Superiority:} Gain 2 victory points for each enemy unit destroyed using a Shoot action.

\subsubsection*{5-4 Frontal Assault:} Gain 2 victory points for each enemy unit destroyed using an Assault action.

\subsubsection*{7-8 Shock and Awe:} Gain 2 victory points for each enemy unit destroyed by the attack of a Heavy unit.

\subsubsection*{9-10 Total Annihilation:} Gain 1 victory point for each enemy unit destroyed.



\section*{Defense}

\subsubsection*{1-2 Attrition:} Gain 6 victory points if you have lost fewer units than the enemy.

\subsubsection*{3-4 Bodyguard:} Gain 6 victory points if you still have any Command units on the battlefield.

\subsubsection*{5-6 Conserve Resources:} Gain 2 victory points for each unit still on the battlefield.

\subsubsection*{7-8 Hold at All Costs:} Choose a piece of terrain outside of your deployment zone. Gain 3 victory points for each Troops unit engaged with that piece of terrain.

\subsubsection*{9-10 Rearguard:} Gain 6 victory points if more of your units are in your deployment zone than there are enemy units outside of their deployment zone.



\section*{Mobility}

\subsubsection*{1-2 Advance:} Gain 1 victory point for every unit not in your deployment zone.

\subsubsection*{3-4 Blitzkrieg:} Gain 2 victory points for each enemy unit destroyed by the attack of a Fast or Spearhead unit.

\subsubsection*{5-6 Breakout:} Gain 3 victory points for each unit that touches the enemy deployment zone's long battlefield edge--at which point that unit is also removed (it does not count as being destroyed).

\subsubsection*{7-8 Take and Hold:} Divide the battlefield into quarters. Gain 3 victory point for each quarter that has at least one of your Troops units in it, and no enemy Troops units in it.

\subsubsection*{9-10 Vanguard:} Gain 2 victory points for each Fast or Spearhead unit not in your deployment zone.



\section*{Raiding}


\subsubsection*{1-2 Blunt the Spearpoint:} Gain 3 victory points for each enemy Spearhead unit destroyed. If the enemy force does not have at least two Spearhead units, roll again.

\subsubsection*{3-4 Big Game Hunter:} Gain 3 victory points for each enemy Heavy unit destroyed. If the enemy force does not have at least two Heavy units, roll again.

\subsubsection*{5-6 Decapitation Strike:} Gain 3 victory points for each enemy Command unit destroyed. If the enemy force does not have at least two Command units, roll again.

\subsubsection*{7-8 Demoralize:} Gain 2 victory points for each enemy unit destroyed as a result of their suppression tokens.

\subsubsection*{9-10 Overburden:} Gain 1 victory point for each enemy unit on the battlefield that has lost half or more of its starting HP, but is not destroyed.




\chapter{Actions and Reactions}

\section*{Move}

The unit may move on the battlefield a number of inches up to its Move characteristic, and rotate upon its center any number of times in any direction. It may not pass through other units or engage enemy units.

If the unit is engaged with any enemy units, it must pass a Will roll; if it fails, it instead gains a Suppression token and makes a Withdraw reaction!

The unit may also use a Move action to voluntarily fall back; in this case it immediately loses a Suppression token and then makes a normal Withdraw reaction towards its deployment table edge.

The unit must end the action in a position where it cannot easily tip over or fall down if accidentally bumped; if this happens anyway because you decided to tempt fate, the unit loses 1 HP!


\section*{Attack}

\subsection*{1. Choose target unit and attack type}
The unit initiates a Shoot or Assault Action against the closest fully or partially visible enemy unit.

\textbf{Shoot:} The attacking unit must not be engaged with enemy units.

\textbf{Assault:} The attacking unit unit makes a Will roll if it is not already engaged with the target unit and the target unit does not have any Suppression tokens. If the roll fails, the action ends. Otherwise, the attacking unit may make a free Move Action during which it must move in a straight line directly toward the target unit. If it does not end up engaged with the target unit, the action ends.


\subsection*{2. Target unit reacts}
The target unit makes a Will roll; if it passes, the unit may optionally make a reaction of its choice. If the roll fails and the target unit is within range of at least one of the attacking unit's weapons, it gains a Suppression token, and if this is an Assault Action, it additionally makes a Withdraw Reaction.


\subsection*{3. Choose weapons and roll attack dice}
The attacking unit may use all weapons capable of hitting the target unit. Add the D values of the chosen weapons together and roll that many dice, discarding dice of your choice in excess of the attacking unit's current HP. If multiple weapons are being used, roll their dice separately or use differently-colored dice for each group of weapons to distinguish them.

The unit's Shoot and Assault characteristics tell you what number must be rolled on the dice.

Each Shoot roll suffers a -1 penalty when shooting at a target unit up to 5'' greater than the weapon's Range, a -2 penalty if the distance is up to 10'' greater, -3 for up to 15'', and so on.

Each successful roll is a hit on the target unit.

If the target unit engaged with any of your units, each failed Shoot roll is resolved as a hit against one of your units (the other player decides which!).


\subsection*{4. Opponent makes Life rolls}
For each hit, the target unit makes a Life roll with a penalty indicated on the weapon used. Each failed Life roll reduces the unit's HP by 1.

If the target unit was only partially visible, it can only lose half its current HP.

If the target unit loses any HP, it makes a Will roll. If it fails, it gains a Suppression token, and if this if this is an Assault Action and/or the unit has lost half or more of its starting HP, it additionally makes a Withdraw Reaction.

If the unit is reduced to 0 HP, it is destroyed.


\section*{Defend}
Place a Defend token next to the unit. While the token is present, attacks targeting this unit suffer -2 Shoot and Assault penalties.



\section*{Cast}
A \textit{Caster} unit not engaged with any enemy units may take a Cast Action to attempt to manifest an arcane power. Choose an arcane power that it knows and a fully or partially visible target unit. The power automatically succeeds unless an enemy Caster unit may attempt to nullify the power by passing a Will roll. If the enemy caster's Will roll passes, the power is nullified. If the power is nullified, the casting unit may attempt to override the nullification; it makes a Will roll of its own, and if that roll passes, the nullification is overridden and the power is cast! The nullifying unit may then attempt to override that with a further Will roll, and so on. All of these Will rolls have \textit{Unstable 2}, and benefit from a bonus equal to the number of HP in the casting or nullifying unit.

This continues until the power is cast, the casting or nullifying unit opts to stop making further Will rolls, or one of the units loses any HP by the \textit{Unstable} rule triggering.

If the power is cast successfully, it unleashes the effects detailed in the power's description.



\section*{Reactions}
At various times, a unit may be permitted or required to make a Reaction. Reactions are always made outside of the normal sequence of activation, regardless of whether the reacting unit had already activated, or how many Actions it has available.


\subsection*{Withdraw}
If a unit has three or more Suppression tokens when it makes a Withdraw Reaction, it is immediately destroyed as its panicked warriors are mowed down, taken prisoner, or become disorganized to the point of combat ineffectiveness!

Otherwise, the unit discards any Defend tokens and makes a Move Action at full speed in a straight line away from whatever enemy unit provoked the Reaction, with no maneuvering or routing around obstacles. If this cannot be done because the unit would become engaged with impassible terrain or any enemy units, the unit is destroyed; cohesion and morale collapse after finding the retreat corridor blocked or filled with enemy forces!


\subsection*{Go to Ground}
The attack targeting this unit suffers -2 Shoot and Assault penalties. Then the unit gains a Suppression token.


\subsection*{Counter-Attack}
This Reaction may only be chosen if the unit being reacted to is not already engaged with any enemy units. The unit makes a free Shoot Action against the unit that provoked the Reaction, but with a -2 Shoot penalty. The target of this attack may not itself make a reaction against it.

If it is reacting to a Shoot action, the free attack is considered to happen simultaneously with the original attack; HP losses take effect only after both attacks are finished. The unit that has suffered greater HP loss gains a Suppression token.

If it is reacting to an Assault Action, all weapons are considered to be used at a distance of 0'' and automatically have visibility during this free Shoot Action. If it is reacting to a unit from inside an \textit{Open} \textit{Transport}, the free Shoot Action targets the building or transport unit they are inside.




\chapter{Weapons}

Every unit has one or more weapons, each with up to five characteristics expressed like so:

\textbf{Assault rifles}: R10'' D(HP) S1 Rapid 2

\textbf{Machine gun}: R20'' D2 S2 Heavy, Rapid 2, Suppressive

\begin{itemize}
    \item \textbf{R[number]:} The range in inches at which target units can be attacked with no Shoot penalty. If there is an ``M'' instead of a number, it is a melee weapon and can only be used in an Assault action.
    \item \textbf{D[number]:} The maximum number of dice the unit can roll when using this weapon. ``(HP)'' means the unit's current HP; if it is divided by a number, round down.
    \item \textbf{S[number]:} The weapon's strength. If present, it becomes a Life penalty when the target unit when it makes its Life rolls.
    \item \textbf{[Special rules]:} This weapon's special rules.
\end{itemize}




\section*{Special weapon rules}

\subsubsection*{AAA:} Ignores Shoot penalties from firing at \textit{Flying} units.

\subsubsection*{Autohit:} Cannot be used against targets farther than its Range, but against other targets, all attack dice automatically succeed.

\subsubsection*{Critical X:} For each attack die that hits and shows an X or lower, the target unit fails a Life roll automatically and loses double the number of HP it normally would. If the weapon would automatically hit, roll anyway.

\subsubsection*{Deadly X:} When striking \textit{Bulky} or \textit{Vehicle/Monster} units, each failed Life roll removes X HP, not just one--but for \textit{Bulky X} units, no more than X per attack die; excess damage is discarded.

\subsubsection*{Indirect Fire X:} Cannot be used against targets closer than X'' away. Can target units that are not visible to its firer, but are fully or partially visible to and within 15'' of a friendly Spearhead or \textit{Scouts} unit, at the cost of a -2 Shoot penalty.

\subsubsection*{Heavy:} Cannot be used if the wielding unit (or a \textit{Transport} containing it) has any Suppression tokens or makes any Move or Assault actions during this round.

\subsubsection*{Rapid/Area Effect/Spray X:} If it hits, the target unit must take X Life rolls, not just one.

If the rule is called ``Area Effect'' or ``Spray,'' it additionally negates Shoot penalties from firing at units with Defend tokens or benefiting from Concealment or Cover. However X becomes limited to the target unit's current HP, and it may not be used in Assault Actions by units that began the round engaged with enemy units.

If the rule is called ``Spray,'' the weapon also negates Life bonuses granted by Cover.

\subsubsection*{Slow:} May only be used once per activation.

\subsubsection*{Sniper:} May be used to fire at units that are not the closest target.

\subsubsection*{Suppressive/Terrifying:} Target unit receives a -2 Will penalty when rolling to make a reaction.

Also, if the rule is called ``Terrifying,'' if the target unit fails its Will roll to make a reaction or after suffering casualties, it makes a Withdraw Reaction after it gains its Suppression token.

\subsubsection*{Unstable X:} For each attack die that shows an X or lower, the unit loses 1 HP. If it would automatically hit, roll anyway.




\chapter{Special Unit Rules}

\subsubsection*{Acute Senses:} \textit{Scout} or \textit{Ambush} units may not be deployed within 15'' of it, and it ignores other units' \textit{Stealth} rule.

\subsubsection*{Ambush:} May optionally not be deployed at the start of the game. Thereafter, at the beginning of any round, you may deploy this unit anywhere at least 5'' away from any enemy units. It may not be the first unit you activate during this round.

\subsubsection*{Bloodthirsty X:} When making an Assault action, does not need to pass a Will roll, and its target unit receives a -X penalty on their Will rolls to be able to make a reaction.

\subsubsection*{Bulky X:} Consumes (current HP * X) spaces in \textit{Transport} units, may not take Defend Actions or Go to Ground Reactions, and does not receive any defensive bonuses from its Suppression tokens. Visibility to or from this unit can only be blocked by other \textit{Bulky} units or \textit{Vehicle/Monster} units.

\subsubsection*{Caster:} May cast and nullify arcane powers and automatically knows the following ones:

\begin{itemize}
    \item \textbf{Arcane Fury:} The target unit is hit with a weapon with the characteristics R10'' D1 L-10 [Deadly 2]
    \item \textbf{War Blessing:} The target unit within 15'' receives +1 Shoot and +1 Assault bonuses during its next activation.
\end{itemize}

See the Cast Action for more details.

\subsubsection*{Fearless:} Does not need to pass a Will roll to make an Assault action. However it may not take Defend Actions or Go to Ground Reactions, and whenever it would receive a Suppression token or make a Withdraw Reaction, instead it loses one HP.

\subsubsection*{Hardened X:} Suffers no penalties on Life rolls against weapons with a Strength of X or less.

\subsubsection*{Jump/Flying X:} Treats all terrain and cover as Clear Terrain for movement purposes.

If the rule is called ``Flying'', then it additionally treats all terrain and cover as Clear Terrain for all other purposes, may not take Defend Actions or Go to Ground Reactions, does not block visibility to other units, and units shooting at it suffer a -X Shoot penalty. Non-\textit{Flying} units may not take Assault actions against it, and may touch or move over its base as though it was not there.

\subsubsection*{Leadership X:} May have up to X Order tokens. A token may be spent by flipping it over at any time to issue an Order to a unit in the same Battle Group within 10'' or sharing a \textit{Transport} with it--but not more than once to the same unit during a round. Orders take effect immediately.

\begin{itemize}
    \item \textbf{Fire discipline!} When making one Shoot Attack, the Ordered unit may fire upon enemy units that are not closest to them, and/or split the fire of its weapons between two enemy units; treat this as two simultaneous Shoot Attacks, each with a subset of the unit's attack dice.
    \item \textbf{Focus!} The Ordered unit automatically passes a Will roll.
    \item \textbf{Give it all you've got!} The Ordered unit gains an extra action during its activation, and then gains two Suppression tokens.
    \item \textbf{Regroup!} The Ordered unit discards a Suppression token.
\end{itemize}

\subsubsection*{Medic/Repair X:} May roll a die at the end of its activation; on a roll of X or less, one HP is restored to this unit or a friendly unit it is engaged with. If the rule is called ``Medic,'' it can only be used on non-\textit{Vehicle} units; otherwise it can only be used on \textit{Vehicle} units.

\subsubsection*{Open:} \textit{Area Effect} and \textit{Spray} weapons inflict double the normal amount of HP loss and immediately make an extra free attack against all its contained units.

If the unit takes fewer than two Move Actions during this round, units contained within it may issue Orders, take Shoot or Cast Actions (measure distances from the closest point on the transport), and may take Assault Actions after leaving.

\subsubsection*{Pathbound:} When moving into, out of, or through Difficult Terrain, you must choose whether to limit the unit's Move to 3, or count the area as Dangerous Terrain 2.

\subsubsection*{Pathfinder:} Treats Difficult, Dangerous, and vertically Impassible Terrain as Clear Terrain.

\subsubsection*{Scouts:} May optionally be deployed last anywhere on the battlefield that leaves it not visible to any enemy units. When it provides visibility for friendly \textit{Indirect Fire} weapons, they do not suffer a penalty on their Shoot rolls.

\subsubsection*{Stealth X:} While benefiting from Concealment or Cover, units shooting at it suffer an additional -X Shoot penalty.

\subsubsection*{Stubborn X:} Counts its number of Suppression tokens as X lower than it really is.

\subsubsection*{Transport X:} May transport any number of other units whose combined HP is X or less. Units may begin the game contained within it. Contained units are considered to be engaged with their \textit{Transport}.

A unit may enter a \textit{Transport} by being engaged with it at the beginning or end of a Move action.

Contained units are removed from the battlefield and may take no Action except to leave using a Move Action: instead of moving normally, the unit is placed on the table engaged with the \textit{Transport}. Units may may not take Assault Actions during the round they leave their \textit{Transport}.

If the \textit{Transport} unit is destroyed, contained units automatically leave and gain a Suppression token.

\subsubsection*{Vehicle/Monster X:} Can never make Will rolls or Reactions, accumulate Suppression tokens, take Defend Actions, enter \textit{Transport} units, or benefit from Concealment. Visibility to or from this unit can only be blocked by other \textit{Vehicle/Monster} units. \textit{Area Effect X} and \textit{Spray X} weapons have X reduced to 1 when firing at this unit.

The unit may target different units with individual weapons, target units that are not the closest one, and use \textit{Heavy} weapons after moving. However it cannot target units that are entirely behind an imaginary line drawn side-to-side through its center.

The unit may turn or rotate only once (at any point) per Move or Assault Action. The unit and enemy units it is engaged with it may use Move Actions to move away from one another without requiring Will rolls. If enemy units fire upon the unit while any of their other units are engaged with it, only die rolls of 1 hit their units instead.

When the unit makes an Assault Action that would bring it into engagement with a non-\textit{Vehicle/Monster} unit, target units may not make a Go to Ground Reaction, and are completely destroyed if they make a Counter-Attack Reaction that is not sufficient to destroy the \textit{Vehicle/Monster} unit!

Finally, when a \textit{Vehicle/Monster} unit is destroyed, do not remove it from the battlefield; it becomes an area of Difficult Terrain and Cover. And if the name of the rule ends with a number, roll a die when when the unit is destroyed; on a roll of X or lower, it explodes or lashes out with its death throes, and every unit within 5'' (including any contained units) must pass as many Life rolls as they have HP. Then remove the unit from the battlefield.




\chapter{Terrain and Cover}

Before a game, the players should classify each area of terrain with the following characteristics. It is permissible for areas of terrain to have multiple classifications; For example a forest would provide both Difficult Terrain and Concealment.




\section*{Clear Terrain}
\textit{For example: open fields, deserts, streets, areas of very sparse vegetation, hilltops}

No barriers to movement or visibility. Clear Terrain is everything not otherwise classified.




\section*{Difficult Terrain}

\textit{For example: infantry going up a hill, cratered ground, ruins, rubble, barricades, sandbag walls, forests, shallow rivers, swamps}

Units receive -2 Move during Move or Assault Actions that take them into, out of, or through areas of Difficult Terrain. Units that move through multiple areas of Difficult Terrain use the worst penalties rather than combining them.

Units without \textit{Bulky} or \textit{Vehicle/Monster} may spend movement distance to ascend and descend within multi-level ruins.






\section*{Dangerous Terrain X}

\textit{For example: minefields, quicksand, rivers}

When a unit moves into, out of, or through Dangerous Terrain, roll a number of dice equal to its HP. For each roll of X or less (a sensible default value is 2), it loses 1 HP.




\section*{Impassible Terrain}

\textit{For example: lava floes, deep water, cliffs}

Units may not move into or through Impassible Terrain. If they are forced to do so for any reason, they are immediately destroyed.




\section*{Concealment}

\textit{For example: tall grass, sparse forests, hedges, fences, smoke,  light wooden or thin metal ruins}

When assessing visibility to a target unit, if 3 or 4 of the lines pass through an area of Concealment, units targeting it suffer a -2 Shoot penalty. This does not apply to targets inside the same area of Concealment as the firing unit, or to units inside an area of Concealment firing at unconcealed targets outside of it.

Visibility cannot be traced through two areas of Concealment; the second one  blocks visibility to all units behind (but not within) it.




\section*{Cover}

\textit{For example: rubble, fortifications, heavy masonry or thick metal ruins, rock piles, trenches, barricades, sandbag walls}

Exactly like Concealment, but a single area blocks visibility to all units behind it, and Units benefiting from the -2 Shoot penalty against them also gain a +2 Life bonus.




\section*{Buildings}

Intact buildings are immobile units with the following characteristics based on their construction:

\begin{itemize}
    \item \textbf{Light:} Life 6, HP 10, Open, Vehicle
    \item \textbf{Heavy:} Life 9, HP 10, Open, Vehicle
    \item \textbf{Fortified:} Life 10, HP 15, Open, Vehicle
\end{itemize}

Due to their size and immobility, they are automatically hit in an Assault action. Units occupying buildings can be assaulted. In this case engaging the building counts as engaging the unit within. The unit in the building counts as being in Cover and may make a reaction for free without needing to pass a Will roll, reflecting the benefits of their prepared and fortified position.




\chapter{Advanced Rules}

These optional rules add additional depth, randomness, and tactical considerations. Feel free to use all, some, or none of them, so long as everyone agrees.



\section*{Advantageous firing positions}

A unit's weapons gain +1 Strength during Shoot actions when any of the following apply:
\begin{itemize}
    \item Entirely behind an imaginary line drawn side-to-side through its center of its target unit
    \item In the opposite battlefield quadrant from the last unit that fired upon its target unit in this round
    \item On terrain that elevates it 2'' or more higher than the base of its target unit
\end{itemize}

Multiple conditions can be applied to the same unit. If a unit's location or elevation are ambiguous, err on the side of applying no penalties.



\section*{Advantages and disadvantages}

Roll a die to determine your Battle Groups' advantages for this battle:
\subsubsection*{1-2 No advantages:} Do not roll for Disadvantages either.

\subsubsection*{3 Heroic figure:} Pick a Command unit; improve the number on its \textit{Leadership} rule and its Shoot, Assault, and HP characteristics by 2.

\subsubsection*{4 Good intel:} You always have the initiative and your units suffer no Shoot penalties when firing at enemy units benefiting from Concealment or Cover.

\subsubsection*{5 Enhanced comms:} \textit{Leadership} units may issue Orders to friendly units with no distance restrictions.

\subsubsection*{6 Cutting-edge equipment:} +1 Strength for all your units' weapons.

\subsubsection*{7 Guerrilla tactics:} Anytime a unit would receive a Suppression token before making a Withdraw reaction, it does not receive that Suppression token. In addition, it is allowed to route around impassible terrain or enemy units while withdrawing.

\subsubsection*{8 Grizzled veterans:} All units gain \textit{Stubborn 1}.

\subsubsection*{9-10 Reinforcements available:} Once per battle when it is your turn to activate a unit, you may bring back a single destroyed Troops unit; place it in its original form anywhere within your deployment zone. It then immediately activates.

\vspace{2em}

...And then roll a die to determine your Battle Groups' disadvantages for this battle:

\subsubsection*{1-2 Exhausted:} All units' Move and Will characteristics are reduced by 1.

\subsubsection*{3-4 Low ammunition:} Every ranged weapon with \textit{Rapid} has its number reduced by 1. Every ranged weapon without \textit{Rapid} (or that is reduced to \textit{Rapid 1}) becomes \textit{Slow}.

\subsubsection*{5-6 Terrible commander:} The unit with the highest \textit{Leadership} has its number reduced to 0.

\subsubsection*{7-8 Desertion:} Roll a die for each of your units one at a time (you may choose the Order) until the die shows a 1; that unit has deserted and is not included in this game.

\subsubsection*{9-10 Poor training:} Every unit suffers permanent -1 Shoot and -1 Assault penalties.



\section*{Battlefield conditions}

Choose a suitably epic battlefield condition from the following list, or determine randomly by rolling a die:

\subsubsection*{1-4 Normal conditions:} Play the game normally.

\subsubsection*{5 Dark/Foggy/Snowing:} Units without \textit{Scouts} or \textit{Acute Senses} count as being inside Dangerous Terrain 2 when they move faster than 5'' during a Move or Assault Action, and the range penalty for shooting becomes -1 for every 3'' away.

\subsubsection*{6 Muddy/Swampy/Snowy:} All areas of Open Terrain become Difficult Terrain. Areas that were already Difficult terrain reduce movement by 3.

\subsubsection*{7 Rainy:} Apply the penalties of both the ``Dark/Foggy'' and ``Muddy/Swampy'' conditions.

\subsubsection*{8 Haunted:} All units except those that are \textit{Fearless} suffer a permanent -1 Will penalty.

\subsubsection*{9 Corrosive atmosphere:} -1 Strength (minimum 0) for all weapons.

\subsubsection*{10 Dangerous ground:} Every time a unit enters a piece of terrain for the first time, roll a die. On a roll of 4 or less, that area of terrain is Dangerous Terrain 3 for the rest of the game. Mark it accordingly so nobody forgets.



\section*{Crippling damage}

When a \textit{Vehicle/Monster} unit loses any wounds from a \textit{Deadly} weapon, roll a die, add the number on the weapon's \textit{Deadly} rule, then consult the following table:

\subsubsection*{3-11 No additional effects}

\subsubsection*{12-14 Immobilized:} The unit's Move characteristic is reduced to 0 for the of the game.

\subsubsection*{15-17 Weapon destroyed:} One of the unit's weapons (chosen by the player who inflicted the damage) may not be used for the rest of the game.

\subsubsection*{18+ Explodes!:} The unit is immediately destroyed and automatically gets a 10 on the die roll to determine what happens after it is destroyed.



\section*{Ignoring Suppression tokens}

When a unit that has any Suppression tokens is activated, it may optionally make a Will roll with a penalty equal to its number of Suppression tokens; if the roll passes, the unit is gripped by extraordinary heroism and ignores the effects of its Suppression tokens during this activation! If the roll fails, it immediately gains an additional Suppression token and then deactivates.



\section*{Random battle events}

At the beginning of each round, the player with the initiative rolls a die. On a roll of 10, one of the following random events immediately occurs. Roll a die to determine which one:

\subsubsection*{1-2 Friendly fire incident:} Your unit that is farthest from any of its Battle Group's Leadership units activates, takes a single Shoot action against its closest friendly unit, gains a Suppression token, and then deactivates.

\subsubsection*{3-4 Breakdown:} Your slowest \textit{Vehicle} unit has broken down or run out of fuel; its Move characteristic is permanently reduced to 0.

\subsubsection*{5-6 Earthquake:} The ground shakes with seismic activity or the thunder of weapons fire. During this round, every unit that moves is considered to be in Dangerous Terrain 3.

\subsubsection*{7-8 Comms are down:} During this round, Leadership units may only issue Orders to units they are engaged with.

\subsubsection*{9-10 ``It's a trap!'':} Pick an enemy unit in an area of terrain. That area becomes Dangerous Terrain 5!

If the event rolled does not apply, roll again.



\section*{Unequal forces}

In real war, most battles do not take place between evenly-matched forces. To simulate this fairly, tor every full 100 point deficit, the smaller force receives an extra victory point for each objective it achieves.

\end{multicols}
\end{document}
